package GUI;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;

import Models.Flight;

public class result extends JFrame {
	public result(){}
	public result(List<Flight> flights) {
		
		List<String> flig = new ArrayList<>();
		for (Flight flight:flights){flig.add(flight.toString());}
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		String[] st=new String[flig.size()];
		String[] a= flig.toArray(st);
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane);
		setLayout(null);
		scrollPane.setBounds(50, 50, 400, 200);
		JList list = new JList(a);
		
		list.setLocation(100,100);
		list.setBounds(50, 50, 200, 300);
		scrollPane.setViewportView(list);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					result frame = new result();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
