import java.net.Socket;
import java.net.InetAddress;
import java.net.Proxy;
import java.net.ServerSocket;
import javax.net.SocketFactory;

class A {
    void foo(SocketFactory factory, String address, int port, InetAddress localAddr, int localPort, boolean stream,
            String host, Proxy proxy, int backlog, InetAddress bindAddr)
            throws Exception {
        new Socket(); // Sensitive.
        new Socket(address, port); // Sensitive.
        new Socket(address, port, localAddr, localPort); // Sensitive.
        new Socket(host, port, stream); // Sensitive.
        new Socket(proxy); // Sensitive.
        new Socket(host, port); // Sensitive.
        new Socket(host, port, stream); // Sensitive.
        new Socket(host, port, localAddr, localPort); // Sensitive.

        new ServerSocket(); // Sensitive.
        new ServerSocket(port); // Sensitive.
        new ServerSocket(port, backlog); // Sensitive.
        new ServerSocket(port, backlog, bindAddr); // Sensitive.

        factory.createSocket(); // Sensitive
    }
}

abstract class mySocketFactory extends SocketFactory { // Sensitive. Review how the sockets are created.
    // ...
}