package Models;

/*
 * A model meant to contain data about a given US airport.
 * Ideally, this class only holds data queried from server.
 */
public class Airport {
	private int gmtOffset; 
	private String code;   
	private String name;  
	
	public Airport(String name, String code, int gmtOffset) {
		this.gmtOffset = gmtOffset;
		this.code = code;
		this.name = name;
	}
	public String toString(){
		return "Airport[code = "+code+",name = "+name+",GMTOffset = "+gmtOffset+"]";
	}
	
	public String getName() { return name; }
}
