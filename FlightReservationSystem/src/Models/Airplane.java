package Models;

public class Airplane {


	String manufacturer;	
	private String model;			
	int totFirst;			
	int totCoach;			
	
	public Airplane(String manufacturer, String model, int totFirst, int totCoach) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.totFirst = totFirst;
		this.totCoach = totCoach;
	}
	public String toString(){
		return "Airplane[manufacturer = "+manufacturer+",model = "+model+",totFirst = "+totFirst+",totCoach = "+totCoach+"]";
	}
}
